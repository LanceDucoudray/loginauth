import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { GalleryComponent } from './component/gallery/gallery.component';
import { ImageDetailComponent } from './component/image-detail/image-detail.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { LoginComponent } from './component/login/login.component';
import { UploadComponent } from './component/upload/upload.component';

import { ImageService } from './shared/image.service';
import { UploadService } from './shared/upload.service';
import { AuthenticationGuard } from './shared/authenticationGuard.service';
import { AuthenticationService } from './shared/authentication.service';

// import { appRoutes } from './routes';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { environment } from './../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    ImageDetailComponent,
    NavbarComponent,
    LoginComponent,
    UploadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    // RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [
    AuthenticationService,
    AuthenticationGuard,
    ImageService,
    UploadService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
