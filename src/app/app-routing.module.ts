import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GalleryComponent } from './component/gallery/gallery.component';
import { AuthenticationGuard } from './shared/authenticationGuard.service';
import { UploadComponent } from './component/upload/upload.component';
import { ImageDetailComponent } from './component/image-detail/image-detail.component';
import { LoginComponent } from './component/login/login.component';


const appRoutes: Routes = [

  { path: 'gallery', component: GalleryComponent, canActivate: [AuthenticationGuard] },
  { path: 'upload', component: UploadComponent, canActivate: [AuthenticationGuard] },
  { path: 'image/:id', component: ImageDetailComponent, canActivate: [AuthenticationGuard] },
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
