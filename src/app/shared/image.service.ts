import { Injectable, Component } from '@angular/core';
import { Observable } from 'rxjs';

import { GalleryImage } from '../component/models/galleryImage.model';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import { FirebaseApp } from 'angularfire2';
import 'firebase/storage';
import * as firebase from 'firebase';

@Injectable()
export class ImageService {
  private uid: string;

galleryImage: AngularFireList<Observable<GalleryImage[]>>;

  constructor(private afAuth: AngularFireAuth, private db: AngularFireDatabase) {
    this.afAuth.authState.subscribe(auth => {
      if (auth !== undefined && auth !== null) {
        this.uid = auth.uid;
      }
    });
  }

  getImages() {
    // return this.db.list('uploads');

    this. galleryImage = this.db.list('aMano');
    // return this.galleryImage.snapshotChanges();
    
  }

}
