import { Component, OnInit, OnChanges } from '@angular/core';
import { ImageService } from 'src/app/shared/image.service';
import { GalleryImage } from '../models/galleryImage.model';
import { Observable } from 'rxjs';
import { AngularFireList } from '@angular/fire/database';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit, OnChanges {

images: AngularFireList<Observable<GalleryImage[]>>;

  constructor(private imageService: ImageService) { }

  ngOnInit() {
    this.images = this.imageService.getImages();
  }

  ngOnChanges(imgs) {
    // this.images = this.imageService.getImages(imgs);
  }

}
