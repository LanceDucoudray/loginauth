import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../shared/authentication.service';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireList } from '@angular/fire/database';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  title: 'Gallery';
  private user: AngularFireList<Observable<firebase.User>>;

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService;
  }
 logout() {
   this.authService.logout().then(onResolve => this.router.navigate['']);
 }
}
 